﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TopologicalAlgorithms
{
    class Sample
    {
        double height;
        Tuple<uint, uint, uint> position;

        public Sample(uint x, uint y, uint z, double height)
        {
            position = Tuple.Create(x, y, z);
            this.height = height;
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.AppendFormat("Sample ({0}, {1}, {2}): {3}", position.Item1, position.Item2, position.Item3, height);

            return result.ToString();
        }
    }

    class Heightfield
    {
        Sample[,,] samples;
        Tuple<uint, uint, uint> dimensions;

        public uint DimX {  get { return dimensions.Item1; } }
        public uint DimY { get { return dimensions.Item2; } }
        public uint DimZ { get { return dimensions.Item3; } }

        public int SampleCount {  get { return samples.Length; } }

        public Heightfield(uint dimX, uint dimY, uint dimZ)
        {
            dimensions = Tuple.Create(dimX, dimY, dimZ);
            samples = new Sample[dimX, dimY, dimZ];
            for (uint x = 0; x < DimX; ++x)
            {
                for (uint y = 0; y < DimY; ++y)
                {
                    for (uint z = 0; z < DimZ; ++z)
                    {
                        samples[x, y, z] = new Sample(x, y, z, 0.0);
                    }
                }
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine("Heightfield");
            result.AppendFormat("\tDimensions: ({0}, {1}, {2}) = {3} samples.\n", DimX, DimY, DimZ, SampleCount);
            result.AppendLine("\tSamples:");
            for (uint x = 0; x < DimX; ++x)
            {
                for (uint y = 0; y < DimY; ++y)
                {
                    for (uint z = 0; z < DimZ; ++z)
                    {
                        result.AppendFormat("\t\t{0}\n", samples[x, y, z]);
                    }
                }
            }

            return result.ToString();
        }
    }
}
